import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { UsersComponent } from './admin/users/users.component';
import { GroupsComponent } from './admin/groups/groups.component';
import {ApplicationComponent} from './admin/application/application.component';
import {SecurityComponent} from './admin/security/security.component';
import {WorkflowComponent} from './admin/workflow/workflow.component';
import {ReportsComponent} from './admin/reports/reports.component';
import {MultifactorComponent } from './admin/security/multifactor/multifactor.component';
import {NoPageFoundComponent } from './admin/no-page-found/no-page-found.component';
import {GroupDetailsComponent } from './admin/groups/group-details/group-details.component';
import {AssignGroupComponent } from './admin/groups/assign-group/assign-group.component';
import {ViewComponent } from './admin/user/profile/view/view.component';
import {WelcomeComponent } from './admin/user/welcome/welcome.component';
import {MainContentComponent } from './admin/main-content/main-content.component';
import { LoginComponent } from './admin/login/login.component';
import { AuthGuard } from './admin/auth/auth.guard';
import { ProfileEditorComponent } from './admin/profile/profile-editor/profile-editor.component';
import { PasswordExpiredComponent } from './admin/signin/password-expired/password-expired.component';
import { ActivateComponent } from './admin/user/activate/activate.component';
import { DeactivateComponent } from './admin/user/deactivate/deactivate.component';
import { DirectoryComponent } from './admin/users/directory/directory/directory.component';


const routes: Routes = [
  {path: '', redirectTo: 'admin/dashboard', pathMatch: 'full'},
  {path:'admin',canActivate:[AuthGuard], component: MainContentComponent, children:[
    {path:'dashboard', component: DashboardComponent},
    {path:'users', component: UsersComponent},
    {path:'user/activate', component: ActivateComponent},
    {path:'user/deactivate', component: DeactivateComponent},
    {path:'groups', component: GroupsComponent},
    {path:'people/directories', component:DirectoryComponent},
    {path: 'user/profile/view/:id', component: ViewComponent },
    {path: 'groups/:id', component: GroupDetailsComponent},
    {path: 'groups/:id/workingSet/:userId', component: AssignGroupComponent},
    {path: 'application', component: ApplicationComponent },
    {path: 'security', component: SecurityComponent },
    {path: 'multifactor', component: MultifactorComponent },
    {path: 'workflow', component: WorkflowComponent },
    {path: 'reports', component: ReportsComponent },
    {path: 'universaldirectory', component: ProfileEditorComponent },
    {path:'', redirectTo: '/admin/dashboard', pathMatch:'full'},
    {path:'**',component:NoPageFoundComponent}

  ]},
  {path:'user/welcome', component: WelcomeComponent},
  {path:'login', component: LoginComponent},
  {path:'signin/password-expired/:id', component: PasswordExpiredComponent},
  {path: '**', component: NoPageFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash : true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
