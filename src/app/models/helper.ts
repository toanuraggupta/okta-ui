export class Helper {
    labelName : string ='Name';
    labelDescription: string='Description (optional)';
    buttonSave : string ='Save';
    buttonUpdate : string ='Update';
    buttonCancel : string ='Cancel';

    // Add peoples form label
    labelFirstName : string ='First Name';
    labelLastName : string = 'Last Name';
    labelUsername : string = 'Username';
    labelEmail : string ='Email';
    labelPassword : string = 'Password';
    labelPrimaryEmail : string = 'Primary email';
    labelSecondaryEmail : string = 'Secondary email (optional)';
    labelGroups : string = 'Groups (optional)';
    labelConfirmPassword : string ='Confirm Password';

    labelUserTypes : string = 'User types';

    optionSetbyUser : string = 'Set by user';
    optionSetbyAdmin : string = 'Set by admin';
    

}

