export class User{
    UserId: number
    FirstName: String
    LastName: string
    UserName: string
    UserEmail: string
    UserStatus:string
}