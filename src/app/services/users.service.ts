import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { Group} from './group.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  userendpoint : any;

  constructor(public configService:ConfigService,private httpClient: HttpClient) {
    this.userendpoint = this.configService.getBakendUrl();
   }
   httpHeader = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'
    })
  }  

   getAllUsers(groupId:string): Observable<Group>{
      return this.httpClient.get<Group>(this.userendpoint+'/groups/'+groupId+'/users')
      .pipe(
        retry(1),
        catchError(this.processError)
      )
   }
   getUsers(): Observable<any>{
    return this.httpClient.get<any>(this.userendpoint+'/users')
    .pipe(
      retry(1),
      catchError(this.processError)
    )
 }
   getSingleUser(userId:string) : Observable<any>{
     return this.httpClient.get<any>(this.userendpoint+'/users/'+userId)
     .pipe(retry(1),catchError(this.processError))
   }

   saveUser(data:any, flag:boolean): Observable<any>{
     
     return this.httpClient.post<any>(this.userendpoint+'/users?activate='+flag,JSON.stringify(data),this.httpHeader)
     .pipe(retry(1),catchError(this.processError))
   }

   saveByAdmin(data:any, falg:boolean) : Observable<any>{ 
     return this.httpClient.post<any>(this.userendpoint+'/users?activate='+falg,JSON.stringify(data),this.httpHeader)
     .pipe(retry(1),catchError(this.processError))
   }

   setByAdminChangePwd(data:any, falg:boolean) : Observable<any>{
    return this.httpClient.post<any>(this.userendpoint+'/users?activate='+falg+'&nextLogin=changepassword',JSON.stringify(data),this.httpHeader)
    .pipe(retry(1),catchError(this.processError))
  }

  changePassword(loginName: string, userData: any): Observable<any>{
    return this.httpClient.post<any>(this.userendpoint+'/users/changePassword/'+loginName,JSON.stringify(userData),this.httpHeader)
    .pipe(retry(1),catchError(this.processError))
  }
  // Activate user -POST

    activateIndividualUser(userId: string) : Observable<any>{
        return this.httpClient.post<any>(this.userendpoint+'/users/'+userId+'/lifecycle/activate',this.httpHeader)
        .pipe(retry(1),catchError(this.processError))
    }

    deactivateUser(userId: string) : Observable<any>{
      return this.httpClient.post<any>(this.userendpoint+'/users/'+userId+'/lifecycle/deactivate',this.httpHeader)
      .pipe(retry(1),catchError(this.processError))
    }

  userSuspend(userId: string) : Observable<any>{
    return this.httpClient.post<any>(this.userendpoint+'/users/'+userId+'/lifecycle/suspend',this.httpHeader)
    .pipe(retry(1),catchError(this.processError))
  }

   updateUser(data:any): Observable<any>{
     return this.httpClient.put<any>(this.userendpoint+'/users',data,this.httpHeader)
     .pipe(retry(1),catchError(this.processError))
   }

   getGroupForUser(userId:string):Observable<any>{
     return this.httpClient.get<any>(this.userendpoint+'/users/'+userId+'/groups')
     .pipe(retry(1),catchError(this.processError))
   }

   addGroupToUser(groupId:string, userId: string):Observable<any>{
      return this.httpClient.post<any>(this.userendpoint+'/users/addGroupToUser/'+groupId+'/'+userId,this.httpHeader)
      .pipe(retry(1),catchError(this.processError))
   }

   uploadWithCSVUsers(file: any) :Observable<any>{
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
     return this.httpClient.post<any>(this.userendpoint+'/users/uploadUsers', formData)
     .pipe(retry(1),catchError(this.processError))
   }

   processError(err) {
    let message = '';
    if(err.error instanceof ErrorEvent) {
     message = err.error.message;
    } else {
     message = `Error Code: ${err.status}\nMessage: ${err.message}`;
    }
    console.log(message);
    return throwError(message);
 }

//  for login process
getImageByUsername(username: string):Observable<any>{
    return this.httpClient.get<any>(this.userendpoint+'/users/getImage/'+username)
    .pipe(retry(1),catchError(this.processError))
}
  
}
