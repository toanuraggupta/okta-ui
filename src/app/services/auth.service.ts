import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private route: Router, private userService: UsersService,
     private httpClient: HttpClient) { }

  setToken(token: string): void{
      localStorage.setItem('token',token);
  }
  getToken(): string{
      return localStorage.getItem('token');
  }
  isLoggedIn(){
    return this.getToken() !== null;
  }
  logout(){
    localStorage.removeItem('token');
    this.route.navigate(['login']);
  }
  
  saveLogin(formData:any, userId: string): Observable<any>{
      return this.httpClient.post<any>(this.userService.userendpoint+'/users/saveLogin/'+userId,
        formData
      ).pipe(retry(1),catchError(this.userService.processError))
  }

  userLogin(loginData: any) : Observable<any>{
      return this.httpClient.post<any>(this.userService.userendpoint+'/users/userLogin',
      {'login':loginData.username,'password':loginData.password},
        this.userService.httpHeader
      )
        .pipe(retry(1),catchError(this.userService.processError))
    }

}

