import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  _uiIp = environment._uiIp;
  _uiPort = environment._uiPort;

  _bakendIp = environment._serverIp;
  _bakendPort = environment._serverPort;

  _uiHttp = environment._uiHttp;
  _bakendHttp = environment._serverHttp;

  _apiEndPoint = environment._endPoint;




  constructor() { }

getUiUrl(){
  return this._uiHttp+'://'+this._uiIp+this._uiPort;
}
getBakendUrl(){
  return this._bakendHttp+'://'+this._bakendIp+this._bakendPort+this._apiEndPoint;
}

}

