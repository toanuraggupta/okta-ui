import { Injectable } from '@angular/core';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';

export class Group{
  profile :{
    name : string;
    description : string;
  }
}

@Injectable({
  providedIn: 'root'
})

export class GroupService {
endpoint;

  constructor(private httpClient : HttpClient, private configService: ConfigService) { 
      this.endpoint = this.configService.getBakendUrl();
      console.log('End Point: '+this.endpoint);
  }
  httpHeader = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
      
    })
  }  

  getGroups(): Observable<any> {
    return this.httpClient.get<any>(this.endpoint +'/groups')
    .pipe(
      retry(1),
      catchError(this.processError)
    )
  }
  addGroups(data:any): Observable<Group> {
    return this.httpClient.post<Group>(this.endpoint +'/groups',JSON.stringify(data), this.httpHeader)
    .pipe(
      retry(1),
      catchError(this.processError)
    )
  }  
  
  getSingleGroup(id:any):Observable<Group>{
    return this.httpClient.get<Group>(this.endpoint+'/groups/'+id)
    .pipe(retry(1),
    catchError(this.processError)
    )
  }
  updateGroup(groupId:string,data:any) : Observable<Group>{
    return this.httpClient.put<Group>(this.endpoint+'/groups/'+groupId,JSON.stringify(data), this.httpHeader)
    .pipe(retry(1),catchError(this.processError))
  }
  deleteGroup(groupId:string){
    return this.httpClient.delete<any>(this.endpoint+'/groups/'+groupId)
        .pipe(retry(1),
        catchError(this.processError))
  }
  addUserToGroup(groupId:string, userIdArray :any) : Observable<any>{
    return this.httpClient.put<any>(this.endpoint+'/groups/'+groupId+'/users/',userIdArray,this.httpHeader)
    .pipe(retry(1),catchError(this.processError))
  }
  getNotMembers(groupId:string):Observable<any>{
    return this.httpClient.get<any>(this.endpoint+'/groups/'+groupId+'/notUsers')
    .pipe(retry(1),catchError(this.processError))
  }

  deleteUserFromGroup(groupId:string, userId:string) : Observable<any>{
    return this.httpClient.delete<any>(this.endpoint+'/groups/'+groupId+'/users/'+userId)
      .pipe(retry(1),catchError(this.processError))
  }
  processError(err) {
    let message = '';
    if(err.error instanceof ErrorEvent) {
     message = err.error.message;
    } else {
     message = `Error Code: ${err.status}\nMessage: ${err.message}`;
    }
    return throwError(message);
 }

}
