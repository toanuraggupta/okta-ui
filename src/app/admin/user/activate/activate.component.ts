import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.css']
})
export class ActivateComponent implements OnInit {
  allActivationList:any[]=[];

  constructor(private userService: UsersService,private router: Router) { }

  ngOnInit(): void {
    this.getAllUsersList();
  }

  allSelected(){

  }
  singleSelected(){

  }

  getAllUsersList(){

    this.userService.getUsers().subscribe((result)=>{

        //this.allActivationList = result;
        //let notActive = result.filter(st => 'SUSPENDED'==st.status);
        // if(result.status !=='ACTIVE'){
        //   //this.allActivationList = result;
        //   console.log(result[0].status)
        // }
        for(let list of result){
          if(list.status !="ACTIVE"){
             this.allActivationList.push(list);
            return;
          }
        }

    },(err: Error)=>{
      console.log(err.message)
    })

  }



}
