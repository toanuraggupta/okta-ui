import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ConfigService } from 'src/app/services/config.service';
import { UsersService } from 'src/app/services/users.service';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit  {
    selectedImage : any;
    imageIds : any[]=[];
    userId: string;
    name: string;
    activationForm;
    
    imagesList : any;
    imagePath : Blob;
    statusErr=false;
    selectChecked : boolean = false;
    invalidForm = false;
  IMAGES: any[] = [
    {'id':1,'caption':'aeonium', 'url':'assets/aeonium-balsamiferum.jpg'},
    {'id':2,'caption':'brooklyn', 'url':'assets/brooklyn-bridge.jpg'},
    {'id':3,'caption':'circuit', 'url':'assets/circuit-board.jpg'},
    {'id':4,'caption':'aurora', 'url':'assets/aurora-borealis.jpg'},
    {'id':5,'caption':'daisy', 'url':'assets/daisy.jpg'},
    {'id':6,'caption':'golden', 'url':'assets/golden-gate.jpg'},
    {'id':7,'caption':'lambs', 'url':'assets/lambs.jpg'},
    {'id':8,'caption':'manta', 'url':'assets/manta-ray.jpg'},
    {'id':9,'caption':'road', 'url':'assets/road.jpg'},
    {'id':10,'caption':'robot', 'url':'assets/robot.jpg'},
    {'id':11,'caption':'sunset', 'url':'assets/sunset-sky.jpg'},
    {'id':12,'caption':'sydney', 'url':'assets/sydney-harbour-bridge.jpg'},
  ];

  constructor(private activateRoute: ActivatedRoute, private auth: AuthService,
    private router: Router, private userService: UsersService,
    private configService: ConfigService) { 
    this.activateRoute.queryParams.subscribe(params =>{
        this.userId = params.id;
        //this.name = params.name;
    })
  }

  ngOnInit(): void {
    
      this.imagesList = this.IMAGES;
      this.getUserNameById(this.userId);
      this.activationForm = new FormGroup({
        newPassword : new FormControl('',[Validators.required,Validators.minLength(8)]),
        confirmPassword : new FormControl('',this.comparePassword.bind(this)),
        recoveryQuestion : new FormControl('',[Validators.required]),
        recoveryAnswer : new FormControl('',[Validators.required]),
        checkbox : new FormControl(''),
      });
      
  }

  comparePassword(control: FormControl): {[key:string]:boolean}{
    if(control.parent){
      const newPassword = control.parent.value['newPassword'];
      const confirmPassword = control.value;
      if(confirmPassword !==newPassword){
        return {passwordMismatch:true}
      }
    }
    return null;
  }

  onSubmit(verifyData: any){
      if(this.activationForm.valid){
        if(this.selectedImage){
          const formData = new FormData();
          formData.append('password',verifyData.newPassword);
          formData.append('question',verifyData.recoveryQuestion);
          formData.append('answer',verifyData.recoveryAnswer);
          formData.append('image',this.selectedImage);
         this.auth.saveLogin(formData, this.userId).subscribe((result)=>{
             this.router.navigate(['admin/users']);

             
         },(err: Error)=>{
           console.log(err.message);
           console.log(JSON.stringify(formData));
           
         })
        }else{
          this.statusErr = true;
          this.invalidForm = false;
        }
      }else{
        this.invalidForm = true;
        this.activationForm.markAllAsTouched();
      }
  }

  onSelectImage(image : any, event: any){
      const lastImage = image.substring(image.lastIndexOf('/')+1);
      for(let img of this.IMAGES){
        if(img.caption==event.currentTarget.id){
          document.getElementById(event.currentTarget.id+1).classList.remove('d-none');
          document.getElementById(event.currentTarget.id).classList.add('active');
          
          this.imagePath = image;
          fetch(this.configService.getUiUrl()+'/assets/'+lastImage)
          .then(res => res.blob())
          .then(blob => {
            this.selectedImage = new File([blob], lastImage, blob)
            console.log('Selected Image :' +this.selectedImage.name)
            this.statusErr = false;
          })
          
        }else{
          document.getElementById(img.caption+1).classList.add('d-none');
          document.getElementById(img.caption).classList.remove('active');
        }
      }

  }

  getUserNameById(userId: string){
    this.userService.getSingleUser(userId).subscribe((result)=>{
      this.name = result.profile.firstName;
    })
  }
  
  get newPassword(){
    return this.activationForm.get('newPassword');
  }
  get recoveryAnswer(){
    return this.activationForm.get('recoveryAnswer');
  }

}
