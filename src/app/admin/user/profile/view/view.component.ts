import { Component, NgModule, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { GroupService } from 'src/app/services/group.service';
import { UsersService } from 'src/app/services/users.service';
interface Group{
  id:string,
  name: string,
  description: string,

}
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  Users:any;
  groupName : string ='';
  groupType : string='';
  userId : string ='';
  groupId : string ='';
  ApplicationInfo : any='';
  profile: string='';
  userGroups : any;
  closeResult : string='';
  firstName: string ='';
  lastName ;
  login;
  groupList : string[] = [];
  control = new FormControl();
  filteredGroups: Observable<string[]>;
  constructor(private activeRouter: ActivatedRoute,
    private modalService : NgbModal, private config: NgbModalConfig,
    private groupService : GroupService,
    public router: Router,
     private userService: UsersService) {
      config.backdrop = 'static';
      config.keyboard = false;
      }

    

  ngOnInit(): void {
    this.userId = this.activeRouter.snapshot.params.id;
    this.getSingleUserInfo(this.userId);
    this.getGroupsByUserId(this.userId);
    this.searchGroup();
    this.filteredGroups = this.control.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );

  }
  private _filter(value: string): string[] {
    const filterValue = this._normalizeValue(value);
    return this.groupList.filter(street => this._normalizeValue(street).includes(filterValue));
  }

  private _normalizeValue(value: string): string {
    return value.toLowerCase().replace(/\s/g, '');
  }
 
  
  getSingleUserInfo(userId:string){
      this.userService.getSingleUser(userId).subscribe((user)=>{
        this.Users = user;
        this.firstName = this.Users?.profile.firstName;
        this.lastName = this.Users?.profile.lastName;
        this.login = this.Users?.profile.login;
      })
  }
  getGroupsByUserId(userId:string){
    this.userService.getGroupForUser(userId).subscribe((groups)=>{
        this.userGroups = groups;
        this.groupName = this.userGroups?.profile?.name;
        console.warn(this.userGroups)
       
    })
  }
  deleteGroup(){
    this.groupService.deleteGroup(this.groupId).subscribe((res)=>{
      this.modalService.dismissAll();
      this.getGroupsByUserId(this.userId);
    })
  }
  deleteUserFromGroup(){
    this.groupService.deleteUserFromGroup(this.groupId,this.userId).subscribe((res:{})=>{
      this.modalService.dismissAll();
      this.getGroupsByUserId(this.userId);
    })
  }

  // Suspend user

  suspendUser(userid:string){
    return this.userService.userSuspend(userid).subscribe((res)=>{
      this.router.navigate(['/admin/users']);
    })
    
  }


  deactivateUser(userid:string){
    return this.userService.deactivateUser(userid).subscribe((res)=>{
      this.router.navigate(['/admin/users']);
    })
  }

  searchGroup(){
      return this.groupService.getGroups().subscribe((result)=>{
        let groups = result;
        for(let group of groups){
          this.groupList.push(group.profile.name+','+group.id);
        }
      })
  }

  addGroupToUser(groupId:string){
      return this.userService.addGroupToUser(groupId,this.userId).subscribe((res)=>{
        this.getGroupsByUserId(this.userId);
      },(err:Error)=>{
        alert(err.message)
      })
  }


  openRemoveModal(content:any, id:string){
    this.groupId = id;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title modal-md' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
     
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
     
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
