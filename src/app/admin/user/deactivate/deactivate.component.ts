import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-deactivate',
  templateUrl: './deactivate.component.html',
  styleUrls: ['./deactivate.component.css']
})
export class DeactivateComponent implements OnInit {
  allDeactivationList: any[]=[];

  constructor(private userService:UsersService) { }

  ngOnInit(): void {
    this.getAllUsersList();
  }
  allSelected(){

  }
  singleSelected(){

  }

  getAllUsersList(){

    this.userService.getUsers().subscribe((result)=>{

        //this.allActivationList = result;
        //let notActive = result.filter(st => 'SUSPENDED'==st.status);
        // if(result.status !=='ACTIVE'){
        //   //this.allActivationList = result;
        //   console.log(result[0].status)
        // }
        for(let list of result){
          if(list.status !="DEPROVISONED"){
             this.allDeactivationList.push(list);
          }
        }

    },(err: Error)=>{
      console.log(err.message)
    })

}
}
