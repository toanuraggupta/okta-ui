import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-multifactor',
  templateUrl: './multifactor.component.html',
  styleUrls: ['./multifactor.component.css']
})
export class MultifactorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
 
  typesOffactors: string[] = [
    'Okta Verify', 'SMS Authentication',
     'Voice Call Authentication', 
     'Google Authenticator', 
     'FIDO2 (WebAuthn)',
     'YubiKey',
     'Symantec VIP',
     'On-Prem MFA',
     'RSA SecurID',
     'Security Question'
];
}

