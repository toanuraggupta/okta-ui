import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-password-expired',
  templateUrl: './password-expired.component.html',
  styleUrls: ['./password-expired.component.css']
})
export class PasswordExpiredComponent implements OnInit {
  
  username : string;
  changeForm : FormGroup;
  

  constructor(private router: Router, private activeRoute: ActivatedRoute,
    private userService : UsersService,
    private auth: AuthService
    ) {
    
   }

  ngOnInit(): void {
    this.username = this.activeRoute.snapshot.params.id;
    this.changeForm = new FormGroup({
      oldPassword: new FormControl('',[Validators.required,Validators.minLength(8)]),
      newPassword : new FormControl('',[Validators.required,Validators.minLength(8),
        Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$")
      ]),
      repeatPassword : new FormControl('',this.comparePassword.bind(this))
    })
    
  }

  onSubmit(){

    if(!this.changeForm.valid){
      this.changeForm.markAllAsTouched();
      
    }else{
      let formData ={
          'oldPassword':this.changeForm.get('oldPassword').value,
          'newPassword': this.changeForm.get('newPassword').value
      }
      this.userService.changePassword(this.username,formData).subscribe((result)=>{
          if(result.action){
            this.router.navigate(['/login']);
          }else {
            console.log(result.errorMsg);
          }
      },(err:Error)=>{
        console.log(err.message);
      })
    }     

  }

  comparePassword(control: FormControl): {[key:string]:boolean}{
    if(control.parent){
      const newPassword = control.parent.value['newPassword'];
      const repeatPassword = control.value;
      if(repeatPassword !==newPassword){
        return {passwordMismatch:true}
      }
      
    }
    
    return null;
    
  }
  
}
