import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl('',[Validators.required]),
    password: new FormControl('',[Validators.required]),
  });

  renderdImg :any;
  errMsg;
  isShowDiv = true;

  constructor(private auth: AuthService, private router: Router,
      private userService : UsersService
    ) { }

  ngOnInit(): void {
    if(this.auth.isLoggedIn()){
      this.router.navigate(['admin']);
    }
  }

  onSubmit(): void{
    if(this.loginForm.valid){
      this.auth.userLogin(this.loginForm.value).subscribe((res)=>{
        if(!res.action){
          
          this.errMsg = res.msg;
        }else if("changepassword"==res.msg){
          let uname = this.loginForm.get('username');
            this.router.navigate(['/signin/password-expired/'+uname.value]);
        }
        else{
          let token = window.btoa(this.loginForm.get('username')+':'+this.loginForm.get('password'));
          this.auth.setToken(token);
          console.log(this.auth.getToken());
          this.router.navigate(['/admin']);
        }
         
      },
      (err: Error)=>{
        console.log(err.message);
      }
      )
    }else{
      this.loginForm.markAllAsTouched();
    }
  }

  /*renderImage():void{
      this.userService.getUsers().subscribe((result)=>{
          for(let users of result){
            if(users.profile.login===this.loginForm.get('email').value){
              this.renderdImg = users.credentials.image;
              return ;
            }
          }
      },(err : Error)=>{
        console.error(err);
      })
  }*/

  getImageOfUser():void{
    this.userService.getImageByUsername(this.loginForm.get('username').value).subscribe((result)=>{
        if(!result){
          this.renderdImg = '';
          this.errMsg = '';
        }else{
          this.renderdImg = result;
        }
    },(err: Error)=>{
      console.error(err.message);
    })
  }

  onToggle():void{
      this.isShowDiv = !this.isShowDiv;
  }

}
