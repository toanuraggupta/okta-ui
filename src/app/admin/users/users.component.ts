import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModule, ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Helper } from 'src/app/models/helper';
import { GroupService } from 'src/app/services/group.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-directory',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  helper = new Helper();
  title = 'Users';
   mySrc;
  closeResult: string = '';
  UsersInfo: any;
  staggedList : any;
  suspendedList: any;
  deactivatedList: any;
  staggedResult : any;
  suspendResult :any;
  deactivateResult: any;
  StaggedInfo : any ='';
  PendingInfo : any ='';
  pendingResult : any;
  pendingList : any;
  ActiveInfo : any ='';
  activeResult : any;
  activeList : any;
  PasswordResetInfo : any ='';
  LockedInfo : any ='';
  SuspendedInfo : any ='';
  DeactivateInfo : any ='';
  searchUserItems : string ='';
  fileName : any;
  file : File;
  userCounts : number = 0;

  staggedCount : number;
  pendingUserActionCount : number;
  activeCount : number;
  pwdResetCount : number;
  lockedOutCount : number;
  suspendedCount : number;
  deActivateCount : number;

  userType : string = 'user';
  userForm = new FormGroup({
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    userName: new FormControl('', [Validators.required]),
    primaryEmail: new FormControl('', [Validators.required,Validators.email]),
    secondaryEmail: new FormControl(''),
    groups: new FormControl(null),
    password: new FormControl('',[Validators.required,Validators.minLength(8)]),
    userType : new FormControl(''),
    terms_user: new FormControl(''),
    terms_admin: new FormControl('')
  });

  groupListItems: any = [];
  userInvalidForm = false;
  constructor(config: NgbModalConfig, private modalService: NgbModal,
    private userService: UsersService, private groupService: GroupService,
    private router: Router) {
    config.backdrop = 'static';
    config.keyboard = false;
  }
 

  

  ngOnInit(): void {
    this.getAllUsersInfo();
    this.groupList();
    this.getStagged();
    this.getPendings();
    this.getActive();
    this.getSuspended();
    this.getDeactivated();
    
    
  }
  addPerson(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title modal-md' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.userForm.reset();
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  openCSVDialog(content:any){
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title modal-md' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
     
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
     
    });
  }

  openActivatePersonDialog(content: any){
    this.modalService.open(content,{ariaLabelledBy: 'modal-basic-title modal-md'}).result.then((result)=>{
      this.closeResult = `Close with: ${result}`;
    }, (reason) =>{
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    })
  }
  // user task activity here!!!
  fileChange(fileInput: any) : void{
    let file = fileInput.target.files[0];
    this.fileName = file.name;
    this.file = file;
    document.getElementById('file').innerText = this.fileName;
  }

  uploadCSVFile(){
      this.userService.uploadWithCSVUsers(this.file).subscribe((res)=>{
        this.modalService.dismissAll();
      })
  }
  getAllUsersInfo() {
    return this.userService.getUsers().subscribe((res) => {
      this.UsersInfo = res;
      let userLengths = this.UsersInfo;
      this.userCounts = userLengths.length;
      this.staggedCount = this.UsersInfo.filter(t => 'STAGED' == t.status).length;
      this.activeCount = this.UsersInfo.filter(t => 'ACTIVE' == t.status).length;
      this.pendingUserActionCount = this.UsersInfo.filter(t => 'PENDING_USER_ACTION' == t.status).length;
      this.lockedOutCount = this.UsersInfo.filter(t => 'LOCKED_OUT' == t.status).length;
      this.suspendedCount = this.UsersInfo.filter(t => 'SUSPENDED' == t.status).length;
      this.deActivateCount = this.UsersInfo.filter(t => 'DEPROVISIONED' == t.status).length;
      this.pwdResetCount = this.UsersInfo.filter(t => 'PASSWORD_EXPIRED' == t.status).length;
    })
  }

  
  getStagged(){
      return this.userService.getUsers().subscribe((result)=>{
          this.staggedResult = result;
          let stagedStatus = this.staggedResult.filter(r => 'STAGED'== r.status);
          if(stagedStatus[0]?.status=='STAGED'){
            this.staggedList = stagedStatus;
            console.log(this.staggedList);
          }
          
      })
  }

  getPendings(){
    return this.userService.getUsers().subscribe((result) =>{
      this.pendingResult = result;
      let pendingStatus = this.pendingResult.filter(p => 'PENDING_USER_ACTION'== p.status);
      if(pendingStatus[0]?.status=='PENDING_USER_ACTION'){
        this.pendingList = pendingStatus;
      }
    })
  }

  getActive(){
    return this.userService.getUsers().subscribe((result) =>{
      this.activeResult = result;
      let activeStatus = this.activeResult.filter(a => 'ACTIVE'==a.status);
      if(activeStatus[0]?.status=='ACTIVE'){
        this.activeList = activeStatus;
      }
    })
  }
  getSuspended(){
    return this.userService.getUsers().subscribe((result) =>{
      this.suspendResult = result;
      let suspendStatus = this.suspendResult.filter(s => 'SUSPENDED'==s.status);
      if(suspendStatus[0]?.status=='SUSPENDED'){
        this.suspendedList = suspendStatus;
      }
    })
  }
  getDeactivated(){
    return this.userService.getUsers().subscribe((result) =>{
      this.deactivateResult = result;
      let deactivateStatus = this.deactivateResult.filter(d => 'DEPROVISIONED'==d.status);
      if(deactivateStatus[0]?.status=='DEPROVISIONED'){
        this.deactivatedList = deactivateStatus;
      }
    })
  }

  saveUsers(data: any) {
    this.userForm.controls['password'].disable();
      if(this.userForm.valid){
        let notFound = false;
        let flags :boolean = false;
      if(this.userType =='admin'){
        this.userForm.controls['password'].enable();
        if(data.password !=''){
          if(data.terms_admin !=true){
            let adminDataFalse = {
              'profile': {
                "firstName": data.firstName,
                "lastName": data.lastName,
                "login": data.userName,
                "email": data.primaryEmail,
                "secondEmail": data.secondaryEmail,
                "mobilePhone": ""
              },
              "groupIds":data.groups,
              "credentials": {
                  "password" : { "value": data.password}
              }
            }
            notFound = true;
            // alert(JSON.stringify(userData)+'Admin by unchecked')
                this.userService.saveByAdmin(adminDataFalse,notFound).subscribe((res) => {
                this.router.navigate(['/admin/users']);
                this.modalService.dismissAll();
                this.userForm.reset();
                this.getAllUsersInfo();
                console.log("Admin results false:="+ res);
            })
          } 
          if(data.terms_admin==true){
            let adminDataTrue = {
              'profile': {
                "firstName": data.firstName,
                "lastName": data.lastName,
                "login": data.userName,
                "email": data.primaryEmail,
                "secondEmail": data.secondaryEmail,
                "mobilePhone": ""
              },
              "groupIds":data.groups,
              "credentials": {
                  "password" : { "value": data.password}
              }
            }
            notFound = true;
            // alert(JSON.stringify(userData)+'Admin by checked')
              this.userService.setByAdminChangePwd(adminDataTrue,notFound).subscribe((res) => {
              this.router.navigate(['/admin/users']);
              this.modalService.dismissAll();
              this.userForm.reset();
              this.getAllUsersInfo();
              console.log("Admin results:="+ res);
            })
          }
         }else{this.userForm.controls['password'].markAsTouched();}
        
      }
      if(this.userType =='user'){
         this.userForm.controls['password'].disable();
            if(data.terms_user==true){
              let userDataTrue = {
                'profile': {
                  "firstName": data.firstName,
                  "lastName": data.lastName,
                  "login": data.userName,
                  "email": data.primaryEmail,
                  "secondEmail": data.secondaryEmail,
                  "mobilePhone": ""
                },
                "groupIds":data.groups
              }
                flags = true;
                this.userService.saveUser(userDataTrue,flags).subscribe((res) => {
                this.router.navigate(['/admin/users']);
                this.modalService.dismissAll();
                this.userForm.reset();
                this.getAllUsersInfo();
                console.log(res);
              })
            }
          if(data.terms_user !=true){
            flags = false;
            let userDataFalse = {
              'profile': {
                "firstName": data.firstName,
                "lastName": data.lastName,
                "login": data.userName,
                "email": data.primaryEmail,
                "secondEmail": data.secondaryEmail,
                "mobilePhone": ""
              },
              "groupIds":data.groups
            }
            this.userService.saveUser(userDataFalse,flags).subscribe((res) => {
              this.router.navigate(['/admin/users']);
              this.modalService.dismissAll();
              this.userForm.reset();
              this.getAllUsersInfo();
              console.log(res);
            })
          }
      }
      }else{
        this.userInvalidForm = true;
        this.userForm.markAllAsTouched();
      }
  
  }

  getUserType(types:any){
    if(types==='admin'){
      document.getElementById('password').classList.remove('d-none');
      document.querySelector('.terms-admin').classList.remove('d-none');
      document.querySelector('.terms-user').classList.add('d-none');
      this.userForm.controls['password'].setValidators(Validators.required);
      this.userForm.controls['password'].enable();
      this.userForm.controls['terms_user'].setValue('');
      this.userForm.controls['terms_admin'].setValue('');
      this.userType = types;
    }else{
      document.getElementById('password').classList.add('d-none');
      document.querySelector('.terms-admin').classList.add('d-none');
      document.querySelector('.terms-user').classList.remove('d-none');
      document.querySelector('.getPassword').remove();
      this.userForm.controls['password'].disable();
      this.userForm.controls['terms_user'].setValue('');
      this.userForm.controls['terms_admin'].setValue('');
      this.userForm.controls['submit'].valid;
      this.userType = types;
    }
  }

  groupList() {
    let groups;
    return this.groupService.getGroups().subscribe((list) => {
      groups = list;
      for (let group of groups) {
        this.groupListItems.push(group);
      }
    })
  }

  // Actvate users when status stagged

  activateUser(userid:string){
    return this.userService.activateIndividualUser(userid).subscribe((res)=>{
      this.router.navigate(['/admin/users']);
      this.modalService.dismissAll();
      this.getAllUsersInfo();
    })
  }

  
  // validation form methods declared here!!!

  get firstName() {
    return this.userForm.get('firstName');
  }
  get getLastName() {
    return this.userForm.get('lastName');
  }
  get getUserName() {
    return this.userForm.get('userName');
  }
  get getPrimaryEmail() {
    return this.userForm.get('primaryEmail');
  }
  get getPassword(){
    return this.userForm.get('password');
  }

}

exports: NgbModule
