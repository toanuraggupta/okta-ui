import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModule,ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup ,FormControl, Validators} from '@angular/forms';
import { GroupService } from 'src/app/services/group.service';
import { Helper } from 'src/app/models/helper';



@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})

export class GroupsComponent implements OnInit {
  helper = new Helper();
  searchValue : string;
  groupForm = new FormGroup({
    name : new FormControl('',[Validators.required]),
    description : new FormControl('NA',[Validators.required])
  });
  Groups : any = [];
  closeResult: string = '';
  constructor(config:NgbModalConfig, private modalService: NgbModal,
     public groupService : GroupService,public router: Router,
     private formBuilder: FormBuilder) {
    config.backdrop ='static';
    config.keyboard = false;
   }

  ngOnInit(): void {
    this.fetchGroups()
    
  }

  fetchGroups(){
    return this.groupService.getGroups().subscribe((res:{}) =>{
      this.Groups = res
      console.warn('result >'+JSON.stringify(this.Groups))
    })
  }

addGroups(data: any) {
  
  let groupData = {
    'profile':data
  }

  alert(JSON.stringify(groupData))
    // this.groupService.addGroups(groupData).subscribe((data: {}) => {
    //   this.router.navigate(['admin/groups']);
    //   this.modalService.dismissAll();
    //   this.fetchGroups();
    // })
  }

  get getName(){
    return this.groupForm.get('name')
  }
  get getDescription(){
    return this.groupForm.get('description')
  }
  openGroupDialog(content:any){
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.groupForm.reset();
    }, (reason) => {
      this.groupForm.reset();
      this.closeResult = `Dismissed ${this.closeGroupDialog(reason)}`;
    });
  }
  private closeGroupDialog(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  openRuleDialog(content:any){
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.closeGroupDialog(reason)}`;
    });
  }

}

