import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Helper } from 'src/app/models/helper';
import { GroupService } from 'src/app/services/group.service';
import { UsersService } from 'src/app/services/users.service';




@Component({
  selector: 'app-group-details',
  templateUrl: './group-details.component.html',
  styleUrls: ['./group-details.component.css']
})
export class GroupDetailsComponent implements OnInit {

  Articles: any;
  page = 1;
  count = 0;
  tableSize = 10;
  tableSizesArr = [10,25,50,100];



  helper = new Helper();
  searchUser : string;
  groupId: string;
  userId : string;
  Users: any;
  Group: any;
  groupName : string;
  groupDescription : string;
  created: string;
  lastUpdated : string;

  updateForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required])
  });
  closeResult: string = '';
  constructor(config: NgbModalConfig, private modalService: NgbModal,
    private activeRoute: ActivatedRoute,
    public groupService: GroupService,
    public userService: UsersService, public router: Router) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit(): void {
    this.groupId = this.activeRoute.snapshot.params.id;
    this.getSingleGroup(this.groupId);
    this.showData();
  }

  showData():void{
    this.getUsersByGroupId(this.groupId);
  }
  tabSize(event){
    this.page = event;
    this.showData();
  }
  tableData(event):void{
    this.tableSize = event.target.value;
    this.page = 1;
    this.showData();
  }
  getSingleGroup(id: string) {
    return this.groupService.getSingleGroup(id).subscribe((res: {}) => {
      this.Group = res;
      this.groupName = this.Group.profile.name;
      this.groupDescription = this.Group.profile.description;
      this.created = this.Group.created;
      this.lastUpdated = this.Group.lastUpdated;
      this.updateForm.controls['name'].setValue(this.Group.profile.name);
      this.updateForm.controls['description'].setValue(this.Group.profile.description);
    })
  }

  getUsersByGroupId(id: string) {
    return this.userService.getAllUsers(id).subscribe((res: {}) => {
      this.Users = res;
        for(let user of this.Users){
          this.userId = user.id;
          console.log(user);
        }
        console.warn("users List >" + JSON.stringify(this.Users))
      
    })
  }
  openEditGroupDialog(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.closeGroupDialog(reason)}`;
    });
  }
  closeGroupDialog(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  updateGroup(data: string) {
    let groupUpdateData = {
      'profile': data
    }
    this.groupService.updateGroup(this.groupId, groupUpdateData).subscribe((data: {}) => {
      this.router.navigate(['admin/groups/' + this.groupId]);
      this.modalService.dismissAll();
      this.getSingleGroup(this.groupId);
    })
  }
  deleteGroup() {
    this.groupService.deleteGroup(this.groupId).subscribe((res: {}) => {
      this.router.navigate(['admin/groups']);
      this.modalService.dismissAll();
      this.groupService.getGroups();

    })
  }
  get getName() {
    return this.updateForm.get('name')
  }
  get getDescription() {
    return this.updateForm.get('description')
  }
  openConfirmDialog(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.closeGroupDialog(reason)}`;
    });
  }

}

