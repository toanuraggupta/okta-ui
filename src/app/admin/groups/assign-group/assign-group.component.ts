import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GroupService } from 'src/app/services/group.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-assign-group',
  templateUrl: './assign-group.component.html',
  styleUrls: ['./assign-group.component.css']
})
export class AssignGroupComponent implements OnInit {
  groupId : string;
  userId : string;
  Groups :any;
  groupName : string;
  allUsers : any;
  groupUsers : any;
  totaluser :number =0;
  groupUserTotal : number =0;
  membersArray : any =[];
  membersIds : any =[];
  membersData : any;
  notMemberData : any; 
  notMembersArray : any =[];
  constructor(public activateRoute: ActivatedRoute,private userService: UsersService,
    public groupService: GroupService, public router:Router) { 
     
    }

  ngOnInit(): void {
    this.groupId = this.activateRoute.snapshot.params.id;
    this.userId = this.activateRoute.snapshot.params.userId;
    this.singleUserInfo(this.userId);
    this.getGroupInfo(this.groupId);
    this.getNotMembers();
    this.getAllUsersByGroupId(this.groupId);
    
  }

  singleUserInfo(userId: string){
    return this.userService.getSingleUser(userId).subscribe((res:{})=>{
    })
  }
  getGroupInfo(groupId:string){
    
    return this.groupService.getSingleGroup(groupId).subscribe((res:{})=>{
       this.Groups = res;
       this.groupName = this.Groups.profile.name;
       
    })
  }
  getNotMembers(){
    return this.groupService.getNotMembers(this.groupId).subscribe((res:{})=>{
        this.allUsers = res;
        for(let userArray of this.allUsers){
            this.notMemberData ={
              id:userArray.id,
              firstname:userArray.profile.firstName,
              lastname:userArray.profile.lastName,
              login:userArray.profile.login
            }
            this.notMembersArray.push(this.notMemberData);
        }
        this.totaluser = this.notMembersArray.length;
        
    })
  }
  getAllUsersByGroupId(groupId:string){
    return this.userService.getAllUsers(groupId).subscribe((res:{})=>{
      this.groupUsers = res;
      
      for(let membersd of this.groupUsers){
        this.membersData ={
          id : membersd.id,
          firstname : membersd.profile.firstName,
          lastname : membersd.profile.lastName,
          login : membersd.profile.login
        }
        this.membersArray.push(this.membersData);
        this.membersIds.push(membersd.id);
       
      }
      this.groupUserTotal = this.membersArray.length;
      
    })
  }
  addAll(){
    for(let adsAll of this.notMembersArray){
      this.membersArray.push(adsAll);
      this.membersIds.push(adsAll.id);
    }
    this.notMembersArray =[];
    this.groupUserTotal = this.membersArray.length;
    this.totaluser = this.notMembersArray.length;
  }
  removeAll(){
    for(let mem of this.membersArray){
      this.notMembersArray.push(mem);
    }
      this.membersIds =[];
      this.membersArray =[];
      this.groupUserTotal = this.membersArray.length;
      this.totaluser = this.notMembersArray.length;
  }

  popSingleItems(id:string){
    let notfound = false;
    for(let membrs of this.membersArray){
      if(membrs.id==id){
        let index = this.membersArray.indexOf(membrs);
        this.notMembersArray.push(membrs);
        if ( index > -1) {
          this.membersArray.splice(index, 1);
          this.membersIds.splice(index,1);
        }
        this.groupUserTotal = this.membersArray.length;
        this.totaluser = this.notMembersArray.length;
        notfound = true;
        break;
      }
    }
    if(!notfound){
      this.membersArray =[];
      this.getAllUsersByGroupId(this.groupId);
    }
    this.groupUserTotal = this.membersArray.length;
    this.totaluser = this.notMembersArray.length;
  }
  pushSingleItems(id:string){
    for(let ntmebrs of this.notMembersArray){
      if(ntmebrs.id==id){
        let index = this.notMembersArray.indexOf(ntmebrs);
        this.membersArray.push(ntmebrs);
        this.membersIds.push(ntmebrs.id);
        if ( index > -1) {
          this.notMembersArray.splice(index, 1);
        }
        this.groupUserTotal = this.membersArray.length;
        this.totaluser = this.notMembersArray.length;
        break;
      }
    }
    
    
  }

  addUserToGroup(){
    return this.groupService.addUserToGroup(this.groupId,this.membersIds).subscribe((res:{})=>{
      this.router.navigate(['/admin/groups/'+this.groupId]);
      this.getNotMembers();
    })
  }
}
