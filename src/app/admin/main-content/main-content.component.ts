import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.css']
})
export class MainContentComponent implements OnInit {

  siteLanguage : string = navigator.languages[2];
  siteLocale : string;
  
  languageList = [
    {code :'en', label: 'en'},
    {code :'es', label: 'ES'},
    {code :'hi', label: 'IN'}
  ];
  constructor(private auth: AuthService){}
  ngOnInit(){
    this.siteLocale = window.location.pathname.split('/')[1];
    this.siteLanguage = this.languageList.find(f => f.code === this.siteLocale).label;
  }

  logout(){
    this.auth.logout();
  }
}
