import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UsersComponent } from './admin/users/users.component';
import { ApplicationComponent } from './admin/application/application.component';
import { SecurityComponent } from './admin/security/security.component';
import { WorkflowComponent } from './admin/workflow/workflow.component';
import { ReportsComponent } from './admin/reports/reports.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { GroupsComponent } from './admin/groups/groups.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MultifactorComponent } from './admin/security/multifactor/multifactor.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule,FormsModule} from '@angular/forms';
import { NoPageFoundComponent } from './admin/no-page-found/no-page-found.component';
import { GroupDetailsComponent } from './admin/groups/group-details/group-details.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AssignGroupComponent } from './admin/groups/assign-group/assign-group.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ViewComponent } from './admin/user/profile/view/view.component';
import { WelcomeComponent } from './admin/user/welcome/welcome.component';
import { UsersService } from './services/users.service';
import { GroupService } from './services/group.service';
import { MainContentComponent } from './admin/main-content/main-content.component';
import { LoginComponent } from './admin/login/login.component';
import { ProfileEditorComponent } from './admin/profile/profile-editor/profile-editor.component';
import { PasswordExpiredComponent } from './admin/signin/password-expired/password-expired.component';
import { ActivateComponent } from './admin/user/activate/activate.component';
import { DeactivateComponent } from './admin/user/deactivate/deactivate.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { DirectoryComponent } from './admin/users/directory/directory/directory.component';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    GroupsComponent,
    ApplicationComponent,
    SecurityComponent,
    WorkflowComponent,
    ReportsComponent,
    DashboardComponent,
    MultifactorComponent,
    NoPageFoundComponent,
    GroupDetailsComponent,
    AssignGroupComponent,
    ViewComponent,
    WelcomeComponent,
    MainContentComponent,
    LoginComponent,
    ProfileEditorComponent,
    PasswordExpiredComponent,
    ActivateComponent,
    DeactivateComponent,
    DirectoryComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatListModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    MatAutocompleteModule,
    MatFormFieldModule,
   
  

  ],
  providers: [UsersService,GroupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
 
