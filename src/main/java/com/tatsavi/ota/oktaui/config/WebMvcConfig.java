package com.tatsavi.ota.oktaui.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig {
    @Bean
    public WebMvcConfigurer forwardToIndex() {
        return new WebMvcConfigurer() {
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                registry.addViewController("/def").setViewName("forward:/index.html");
                registry.addViewController("/hi").setViewName("forward:/hi/index.html");
                registry.addViewController("/es").setViewName("forward:es/index.html");
            }
        };
    }

}
