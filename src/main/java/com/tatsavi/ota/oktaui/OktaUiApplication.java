package com.tatsavi.ota.oktaui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEurekaClient
public class OktaUiApplication {

	private static Logger logger = LoggerFactory.getLogger(OktaUiApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(OktaUiApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(final ApplicationContext context){
		return args -> {
			String [] beanNames = context.getBeanDefinitionNames();
			for(String beanName : beanNames){
				logger.info(beanName);
			}
		};
	}
}
