package com.tatsavi.ota.oktaui.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Locale;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class LocalizedController {

    private static Logger logger = LoggerFactory.getLogger(LocalizedController.class);

    @RequestMapping(value = "/", method = GET)
    public ModelAndView getLocaleName(Locale locale) {
        logger.info("Got request for OKTA UI from locale {}-{} ", locale.getLanguage(), locale.getCountry());
        return "IN".equalsIgnoreCase(locale.getCountry()) ?  new ModelAndView("forward:/hi") :
                "ES".equalsIgnoreCase(locale.getCountry()) ?  new ModelAndView("forward:/es")
                        : new ModelAndView("forward:/def");
    }
}