export const environment = {
  production: true,
  _uiIp :'173.249.45.182',
  _serverIp :'173.249.45.182',

  _uiHttp :'https',
  _serverHttp:'https',

  _uiPort:':4200',
  _serverPort:':9000',
  
  _endPoint:'/api/v1',
};
